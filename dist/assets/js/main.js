'use strict';

jQuery(document).ready(function ($) {
  /* ========================================================================= *\
  **  === MAIN SCRIPT                                                          *| 
  \* ========================================================================= */
  // BURGER MENU
  $('button.hamburger').click(function () {
    $('button.hamburger').toggleClass('is-active');
    $(".main-menu-wrapper").toggleClass('is-open');
  });

  /* ========================================================================= *\
  **  === SCROLL REVEALS                                                       *| 
  \* ========================================================================= */
  var $status2 = $('.pagingInfo');
  var $slickElement2 = $('.caroussel--slider');

  $slickElement2.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
    var i = (currentSlide ? currentSlide : 0) + 1;
    $status2.html('<div class="counter"><span class="change">' + i + '</span>' + '<span class="total"> / ' + slick.slideCount + '</span></div>');
  });

  $('.caroussel--slider').slick({
    dots: false,
    infinite: true,
    speed: 300,
    slidesToShow: 1,
    adaptiveHeight: true,
    prevArrow: $('.prev'),
    nextArrow: $('.next')
  });

  $('.nav--hamburger-container').click(function () {
    $('.nav--menu-full').addClass('is-active');
  });

  $('.nav--close-container').click(function () {
    $('.nav--menu-full').toggleClass('is-active');
  });

  /* ========================================================================= *\
  **  === NAV                                                                  *| 
  \* ========================================================================= */

  var largeur_fenetre = $(window).width();
  var hauteur_fenetre = $(window).height();

  $(window).on("scroll mousemove", function () {
    var scroll = $(window).scrollTop();
    if (scroll <= 5) {
      $('#test').css('top', 0);
      $('#test').css('clip-path', "ellipse(45% 50% at 50% 50%)");
    } else {
      $('#test').css('top', event.pageY - scroll - hauteur_fenetre * 50 / 100);
      if (event.pageX >= largeur_fenetre - 50) {
        $('#test').css('clip-path', "ellipse(45% 50% at 50% 50%)");
      } else {
        $('#test').css('clip-path', "ellipse(25% 50% at 50% 50%)");
      };
    }
  });

  /* ========================================================================= *\
  **  === BACK TO TOP                                                          *| 
  \* ========================================================================= */
  $("#totop").click(function () {
    $("html, body").animate({
      scrollTop: 0
    }, 1000);
  });

  /* ========================================================================= *\
  **  === BUBUUUUULLLES                                                        *| 
  \* ========================================================================= */
  // $("#bubulle").event("mouseover" )
  // $('.img-link--link').hover( function(event) {
  //   $(this).toggleClass('shape');
  // });

  /* ========================================================================= *\
  **  === GSAP                                                                 *| 
  \* ========================================================================= */
  gsap.registerPlugin(ScrollTrigger);

  // animations apparitions
  function animateFrom(elem, direction) {
    var direction = direction | 1;

    var x = 0;
    var y = direction * 10;
    if (elem.classList.contains("reveal-left")) {
      x = -50;
      y = 0;
    } else if (elem.classList.contains("reveal-right")) {
      x = 50;
      y = 0;
    } else if (elem.classList.contains("reveal-delay")) {
      x = 0;
      y = direction * 10;
    }
    gsap.fromTo(elem, {
      x: x,
      y: y,
      autoAlpha: 0
    }, {
      duration: 1.25,
      x: 0,
      y: 0,
      autoAlpha: 1,
      ease: "expo",
      overwrite: "auto"
    });
  }

  // fonction hide qui cache les element pour les faire apparaitres
  function hide(elem) {
    gsap.set(elem, {
      autoAlpha: 0
    });
  }

  // calback reveal permet d'afficher les elements en faisant appel à la fonction animateFrom()
  gsap.utils.toArray(".reveal").forEach(function (elem) {
    hide(elem);

    ScrollTrigger.create({
      trigger: elem,
      onEnter: function onEnter() {
        animateFrom(elem);
      },
      onEnterBack: function onEnterBack() {
        animateFrom(elem, -1);
      },
      onLeave: function onLeave() {
        hide(elem);
      } // assure that the element is hidden when scrolled into view
    });
  });

  gsap.utils.toArray(".parallax-bg").forEach(function (elem) {
    gsap.to(elem, {
      scrollTrigger: {
        trigger: elem,
        scrub: true,
        start: "top"
        // end: "bottom bottom",
        // markers: true,
      },
      yPercent: 20
    });
  });

  gsap.utils.toArray(".parallax-10").forEach(function (elem) {
    gsap.to(elem, {
      scrollTrigger: {
        trigger: elem,
        scrub: 0.4
        // markers: true,
      },
      ease: "none",
      yPercent: 15 //move each box 500px to right
    });
  });
});